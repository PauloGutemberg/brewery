//
//  NetworkManager.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 03/03/21.
//

import Foundation
import Moya

protocol NetworkManagerOutput {
    func getBreweryGroup(breweries: [BreweryBO])
    func getError(message: String)
}

class NetworkManager: NSObject {
 
    private static var providerGenuine = MoyaProvider<Brewery>()
    private static var providerMock = MoyaProvider<Brewery>(stubClosure: MoyaProvider.delayedStub(2.0))
    
    var output: NetworkManagerOutput
    
    init(output: NetworkManagerOutput) {
        self.output = output
    }
    
    func get(endpoint: Brewery){
        NetworkManager.providerMock.request(endpoint) { (result) in
            switch result {
            case .success(let response):
                guard let breweries = try? response.map([BreweryBO].self)  else {
                    self.output.getError(message: "Sorry, we had an error processing the response")
                    return
                }
                self.output.getBreweryGroup(breweries: breweries)
            case .failure( _):
                self.output.getError(message: "Sorry, we had an error processing the response")
            }
        }
    }
}

/*
 
 class NetworkManager: NSObject {
     
     static var provider = MoyaProvider<MyCocktail>()
     
         
     static func get(endpoint: MyCocktail, handler: @escaping (Response?, Error?) -> ()){
         provider.request(endpoint) { (result) in
             switch result {
             case .success(let response):
                 /*print(response.statusCode)
                 print(response.request?.description ?? "url error")
                 print(String(bytes: response.data, encoding: .utf8)!)*/
                 handler(response , nil)
                 
             case .failure(let error):
                 handler(nil, ErrorLocalized.localizedError(localizedDescription: error.localizedDescription))
             }
         }
     }
     
 }

 */
