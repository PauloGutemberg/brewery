//
//  Requester.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 03/03/21.
//

import Foundation

protocol RequesterOutput {
    
}

class Requester: NSObject {
    
    var output: RequesterOutput
    
    init(output: RequesterOutput) {
        self.output = output
    }
    
    func getBreweries(by cityName: String){
        NetworkManager(output: self).get(endpoint: .listBreweriesByCity(cityName: cityName))
    }
}

extension Requester: NetworkManagerOutput {
    
    func getBreweryGroup(breweries: [BreweryBO]) {
        print(breweries)
    }
    
    func getError(message: String) {
        print(message)
    }
}

/*
 static func getCategories(completionHandler completion:@escaping (_ success:Bool, _ error : NSError?) -> Void){
     
     NetworkManager.get(endpoint: .categories) { (response, errorResponse) in
         
         if let responseAux = response {
             let categoriesServerModel = try! responseAux.map([CategoryResponse].self, atKeyPath: "drinks")
             for categoryServerModel in categoriesServerModel {
                 if let nameCategory = categoryServerModel.strCategory {
                     let categoryAux = Category(strCategory: nameCategory)
                     self.arrayCategories.append(categoryAux)
                 }
             }
             
             Singleton.sharedInstance.categories = self.arrayCategories
             completion(true, nil)
         
         } else if let error = errorResponse {
             print(error.localizedDescription)
             completion(false, nil)
         }
     }
 }

 */
