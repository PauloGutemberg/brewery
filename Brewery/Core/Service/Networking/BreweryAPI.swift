//
//  BreweryAPI.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 03/03/21.
//

import Foundation
import Moya

enum Brewery {
    case listBreweriesByCity(cityName: String)
}

extension Brewery: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.openbrewerydb.org")!
    }
    
    var path: String {
        switch self {
        case .listBreweriesByCity:
            return "breweries"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        switch self {
        case .listBreweriesByCity(cityName: _):
            let mock = MockData.getBreweries()
            guard let mockData = mock.dataEncoded else { return Data() }
            return mockData
        }
    }
    
    var task: Task {
        switch self {
        case .listBreweriesByCity(cityName: let cityName):
            return .requestParameters(parameters: ["by_city": cityName], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    

}
