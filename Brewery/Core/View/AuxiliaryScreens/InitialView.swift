//
//  InitialView.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/03/21.
//

import UIKit
import SnapKit

class InitialView: UIView, CodableView {
    
    var motherView: UIView
    
    override init(frame: CGRect) {
        self.motherView = UIView()
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        self.motherView.backgroundColor = UIColor.green
    }
    
    func buildViews() {
        self.addSubview(self.motherView)
    }
    
    func setupConstraints() {
        self.motherView.snp.makeConstraints { (make) in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
    }
}
