//
//  BaseViewController.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/03/21.
//

import UIKit

open class BaseViewController<V: CodableView>: NiblessViewController where V: UIView {

    override open var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    weak var mainCoordinator: MainCoordinator?
    
    public var viewAssociated: V! {
        return self.view as? V
    }
    
    open override func loadView() {
        self.view = V()
    }
    
    convenience init(mainCoordinator: MainCoordinator) {
        self.init()
        self.mainCoordinator = mainCoordinator
    }
}
