//
//  InitialViewController.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/03/21.
//

import UIKit

class InitialViewController: BaseViewController<InitialView> {

    override func viewDidLoad() {
        super.viewDidLoad()
        InitialViewPresenter(output: self).getBreweryGroup(by: "san diego")
    }

}

extension InitialViewController: InitialViewPresenterOutput {
    
}
