//
//  InitialViewPresenter.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 05/03/21.
//

import Foundation

protocol InitialViewPresenterOutput {
    
}

class InitialViewPresenter: NSObject {
    
    var output: InitialViewPresenterOutput
    init(output: InitialViewPresenterOutput) {
        self.output = output
    }
    
    func getBreweryGroup(by cityName: String){
        Requester(output: self).getBreweries(by: cityName)
    }
    
}

extension InitialViewPresenter: RequesterOutput {
    
}
