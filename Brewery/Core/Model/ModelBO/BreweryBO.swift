//
//  BreweryBO.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 05/03/21.
//

import Foundation

class BreweryBO: Codable {
    
    let id: Int?
    let name, breweryType, adress, longitude, latitude, phone, website, logo : String?
    let rating: Double?
   

    enum CodingKeys: String, CodingKey {
        case id
        case breweryType = "brewery_type"
        case adress, longitude, latitude, phone, rating, website, logo, name
    }

    init(id: Int?, logo: String?, name: String?, breweryType: String?, adress: String?, longitude: String?, latitude: String?, phone: String?, rating: Double?, website: String?) {
        self.id = id
        self.logo = logo
        self.name = name
        self.breweryType = breweryType
        self.adress = adress
        self.longitude = longitude
        self.latitude = latitude
        self.phone = phone
        self.rating = rating
        self.website = website
    }
}
