//
//  GeneralProtocols.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/03/21.
//

import Foundation

public protocol CodableView: class {
    func setupComponents()
    func buildViews()
    func setupConstraints()
}

protocol Coordinator {
    func start()
}

extension CodableView {
    func setupView(){
        setupComponents()
        buildViews()
        setupConstraints()
    }
}
