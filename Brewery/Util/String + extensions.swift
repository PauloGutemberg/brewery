//
//  String + extensions.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 05/03/21.
//

import Foundation

extension String {
    
    var dataEncoded: Data? {
        return self.data(using: String.Encoding.utf8)
    }
    
}
