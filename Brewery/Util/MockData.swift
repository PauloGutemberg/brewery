//
//  MockData.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 05/03/21.
//

import Foundation

final class MockData {
    
    static func getBreweries() -> String {
        let brewery: String = "[{\"id\":299,\"logo\":\"then put a website here\",\"name\":\"Almanac Beer Company\",\"brewery_type\":\"micro\",\"adress\":\"651B W Tower Ave\",\"longitude\":\"-122.306283180899\",\"latitude\":\"37.7834497667258\",\"phone\":\"4159326531\",\"rating\":2.5,\"website\":\"then put a website here\"}]"
        return brewery
    }

}
