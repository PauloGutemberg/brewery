//
//  ConstructorController.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/03/21.
//

import UIKit

class ConstructorController<Controller: UIViewController, ParametersController> {}

extension ConstructorController where Controller == InitialViewController, ParametersController == InitialViewControllerModel {
    
    static func criaController(_ parameters: InitialViewControllerModel, mainCoordinator: MainCoordinator) -> Controller{
        let controller = Controller(mainCoordinator: mainCoordinator)
        return controller
    }
}
