//
//  MainCoordinator.swift
//  Brewery
//
//  Created by Paulo Gutemberg de Sousa Cavalcante on 04/03/21.
//

import UIKit

//MARK: main class coordinator
final class MainCoordinator {
    
    var navigationController: UINavigationController?
    var window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func openInitialViewController(_ parameters: InitialViewControllerModel){
        let controller = ConstructorController<InitialViewController, InitialViewControllerModel>.criaController(parameters, mainCoordinator: self)
        openWithRootViewController(controller: controller)
    }
    
    private func openWithRootViewController(controller: UIViewController){
        self.window.rootViewController?.dismiss(animated: false, completion: nil)
        self.window.makeKeyAndVisible()
        self.window.rootViewController = controller
    }
}

//MARK: main class coordinator
extension MainCoordinator: Coordinator {
    func start() {
        openInitialViewController(InitialViewControllerModel())
    }
}
